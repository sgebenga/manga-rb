require 'sinatra/base'
require_relative './application_controller'

class AdminController < ApplicationController

 get '/login' do
   "admin login"
 end 

 get '/logout' do
   "admin logout"
 end 

end
