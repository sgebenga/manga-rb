require 'sinatra'
require 'sinatra/reloader' if development?
require 'mongoid'
require 'slim'
require 'redcarpet'
require './controllers/application_controller'
require './models/article'

class Application < ApplicationController

 configure do
  Mongoid.load!('./mongoid.yml')
  enable :sessions
  #set :static, true
  #set :public_folder, File.expand_path("..",Dir.pwd)  + '/public'
  set :root , File.expand_path(Dir.pwd) 
  puts "root in application : " + root
 end

 helpers do
  def admin?
   session[:admin]
 end
 end

 get '/?' do
  redirect to("/articles")
 end

get '/articles' do
 @articles = Article.all
 @title = "manga : articles"
 slim :index
end

get '/articles/new'do
 @article = Article.new
 slim :new
end

post '/articles' do
 article = Article.create(params[:article])
 redirect to("/articles/#{article.id}")
end

get '/articles/:id' do
 @article = Article.find(params[:id])
 @title = @article.title
 slim :show
end

get '/articles/:id/edit' do
 @article =Article.find(params[:id])
 slim :edit
end

put '/articles/:id' do
 article = Article.find(params[:id])
 article.update_attributes(params[:article])
 redirect to("/articles/#{article.id}")
end

get '/articles/delete/:id' do
 @article = Article.find(params[:id])
 slim :delete
end

delete '/articles/:id' do
 Article.find(params[:id]).destroy
 redirect to("/articles")
end
end
