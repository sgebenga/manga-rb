require 'sinatra/base'
require './helpers/application_helper'

class ApplicationController < Sinatra::Base
  helpers ApplicationHelper

  # set folder for templates to ../views, making the path absolute
  set :root , File.expand_path(Dir.pwd)
  set :views, File.expand_path('../../views',__FILE__)

  configure :production , :development  do
   enable :logging
   puts "root in application_controller: " + root
  end

  not_found do
    title 'Not found!'
    "Not found!!"
  end
end
