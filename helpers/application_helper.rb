module ApplicationHelper
 def title(value = nil)
  @title = value if value
  @title ? "Big App 2013 - #{@title}" : "Big App 2013"
 end
end
