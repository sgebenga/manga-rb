#require './app'

#require './controllers/admin_controller'
#require './controllers/api_controller'

require './boot'
Dir.glob('./{helpers,controllers}/*.rb').each { |file| require file }

#set :public_folder, File.dirname(__FILE__) + '/public'

map('/') do
 run Application
end

map('/admin/') do
 run AdminController
end

map('/api/') do
 run ApiController
end


