require 'sinatra/base'
require './controllers/application_controller'

class Boot < ApplicationController

 configure do
  set :root, File.dirname(__FILE__)
  set :public_folder, File.dirname(__FILE__) + '/public'
  puts "root in boot: " + root
 end

 get '/contact' do
  slim :contact
 end
end
